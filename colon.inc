%define entry 0
%macro colon 2
%ifid %2
    %%nextentry: dq entry
%else
    %fatal "only identifiers supported as labels"
%endif
%ifstr %1
    db %1, 0
    %2:
%else
    %fatal "only string supported as key's value"
%endif
%define entry %%nextentry
%endmacro

