section .text
%include 'words.inc'
%define BUFFER_SIZE 256

global _start

extern read_word
extern print_string
extern find_word
extern print_newline
section .data

reading_success:
db 'SUCCESS READING', 10, 0

unable_to_reading_message:
db 'READING ERROR', 10, 0

founded_message:
db 'KEY FOUND', 0

search_error:
db 'NO SUCH KEY', 10, 0
section .text

_start:
    sub rsp, BUFFER_SIZE
    mov rsi, BUFFER_SIZE
    mov rdi, rsp
    call read_word
    test rax, rax
    jz .unable_to_read

    mov rdi, rax
    mov rsi, entry
    call find_word
    test rax, rax
    jz .unable_to_find
    push rax
    mov rdi, founded_message
    mov rsi, 1
    call print_string
    pop rax
    mov rdi, rax
    mov rsi, 1
    call print_string
    call print_newline
    jmp .end

.unable_to_find:
    mov rdi, search_error
    mov rsi, 2
    call print_string
    jmp .end
.unable_to_read:
    mov rdi, unable_to_reading_message
    mov rsi, 2
    call print_string
    jmp .end

.end:
    call exit

