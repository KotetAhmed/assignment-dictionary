section .text
global find_word

extern string_length
extern string_equals

find_word:
.loop:    
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    test rax, rax
    jnz .found

    mov rsi, [rsi]
    test rsi, rsi
    jz .fail
    jmp .loop

.fail:
    xor rax, rax
    ret

.found:
    add rsi, 8
    push rsi
    call string_length
    pop rsi
    add rax, rsi
    inc rax
    ret
